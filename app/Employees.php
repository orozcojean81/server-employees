<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    protected $table = 'employees';
    protected $primaryKey = 'id';
    protected $fillable = [ 'name', 'other_names', 'surname', 'second_surname', 'country',
    'type_id', 'number_id', 'email', 'date_admission', 'area', 'condition' ];
}
