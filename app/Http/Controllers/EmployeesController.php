<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employees;
use App\Countries;
use App\Areas;
use App\Type_id;

class EmployeesController extends Controller
{
    public function registerEmployee(Request $request){


        $input = $request->only('name', 'other_names', 'surname', 'second_surname', 'country', 'type_id', 'number_id',
        'date_admission', 'area');
        $validate = $this->validateInput($input);

        if( $validate ){
            return response()->json([
                'status' => false,
                'message' => 'Error in validation',
                'error' => $validate
            ], 500);
        }

        $validateNumberDocument = $this->validateNumberIdAndTypeId($request->number_id, $request->type_id);
        if( $validateNumberDocument ){
            return response()->json([
                'status' => false,
                'message' => $validateNumberDocument,
            ], 500);
        }

        $email = $this->createEmail($request);
        if($email == 'Error creating email'){
            return response()->json([
                'status' => false,
                'message' => $email,
            ], 500);
        }


        $newEmployee = new Employees();
            $newEmployee->name = $request->name;
            $newEmployee->other_names = $request->other_names;
            $newEmployee->surname = $request->surname;
            $newEmployee->second_surname = $request->second_surname;
            $newEmployee->country = $request->country;
            $newEmployee->type_id = $request->type_id;
            $newEmployee->number_id = $request->number_id;
            $newEmployee->email = $email;
            $newEmployee->date_admission = $request->date_admission;
            $newEmployee->area = $request->area;
            $newEmployee->condition = 1;
        $newEmployee->save();

        try{
            return response()->json([
                'status' => true,
                'message' => 'Registered Successfully',
                'email created' => $email
            ], 200);
        }catch(\Exception $e){
            return response()->json([
                'status' => false,
                'message' => 'Error in server',
                'error' => $e->getMessage()
            ], 500);
        }
    }

    private function validateInput($input){

        $validation = \Validator::make($input, [
            'name' => 'required|max:20',
            'other_names' => 'max:50',
            'surname' => 'required|max:20',
            'second_surname' => 'required|max:20',
            'country' => 'required',
            'type_id' => 'required',
            'number_id' => 'required',
            'date_admission' => 'required|date',
            'area' => 'required'
        ]);

        if( $validation->fails() ){
            return $validation->messages();
        }

    }

    private function validateNumberIdAndTypeId($number_id, $type_id){
        $employee = Employees::select('id')
        ->where('number_id', '=', $number_id)
        ->where('type_id', '=', $type_id)
        ->get();

        if( count($employee)> 0 ){
            return 'The number id already exists';
        }
    }

    private function createEmail($input){

        $domain = $input->country == 1 ? 'cidenet.com.co' : 'cidenet.com.us';
        
        $employees = Employees::select('id')
        ->where('name', '=', $input->name)
        ->where('surname', '=', $input->surname)
        ->where('country', '=', $input->country)
        ->get();

        $name = str_replace(" ", "", strtolower($input->name));
        $surname = str_replace(" ", "", strtolower($input->surname));
        $consecutive = count($employees);

        $email = count($employees) == 0 ? "{$name}.{$surname}@{$domain}" : 
        (count($employees)  == 1 ? "{$name}.{$surname}1@{$domain}" :
        "{$name}.{$surname}{$consecutive}@{$domain}");


        try{
            return $email;
        }catch(\Exception $e){
            return 'Error creating email';
        }

    }

    public function getEmployees(){
        $employees = Employees::join('areas', 'employees.area', '=', 'areas.id')
        ->join('type_id', 'employees.type_id', '=', 'type_id.id')
        ->join('countries', 'employees.country', '=', 'countries.id')
        ->select('employees.*', 'countries.description as country', 'areas.description as area', 'type_id.description as type_id')
        ->paginate(10);

        return response()->json([
            'status' => true,
            'employees' => $employees
        ], 200);
    }


    public function getInformationByEmployee($id){

        $employee = Employees::select('*')
        ->where('id', '=', $id)
        ->get();

        return response()->json([
            'status' => true,
            'information' => $employee[0]
        ], 200);

    }

    public function edditInformationEmployee(Request $request){

        $input = $request->only('name', 'other_names', 'surname', 'second_surname', 'country', 'type_id', 'number_id',
        'date_admission', 'area');
        $validate = $this->validateInput($input);

        if( $validate ){
            return response()->json([
                'status' => false,
                'message' => 'Error in validation',
                'error' => $validate
            ], 500);
        }
        $eddit = Employees::findOrFail($request->id);


        if( $eddit->number_id != $request->number_id || $eddit->type_id != $request->type_id){
            $validateNumberDocument = $this->validateNumberIdAndTypeId($request->number_id, $request->type_id);
            if( $validateNumberDocument ){
                return response()->json([
                    'status' => false,
                    'message' => $validateNumberDocument,
                ], 500);
            }
        }



        if( $eddit->name != $request->name || $eddit->surname != $request->surname || $eddit->country != $request->country ){
            $email = $this->createEmail($request);
            
            if($email == 'Error creating email'){
                return response()->json([
                    'status' => false,
                    'message' => 'Error creating new email',
                ], 500);
            }
        }else{
            $email = $eddit->email;
        }

        $eddit->name = $request->name;
        $eddit->other_names = $request->other_names;
        $eddit->surname = $request->surname;
        $eddit->second_surname = $request->second_surname;
        $eddit->country = $request->country;
        $eddit->type_id = $request->type_id;
        $eddit->number_id = $request->number_id;
        $eddit->email = $email;
        $eddit->area = $request->area;
        $eddit->save();


        try{
            return response()->json([
                'status' => true,
                'message' => 'Updated successfully',
                'email' => $email
            ], 200);
        }catch(\Exception $e){
            return response()->json([
                'status' => false,
                'message' => 'Error in serve',
                'error' => $e->getMessage()
            ], 200);
        }
    }
}
