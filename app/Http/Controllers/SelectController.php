<?php

namespace App\Http\Controllers;
use App\Countries;
use App\Areas;

use Illuminate\Http\Request;

class SelectController extends Controller
{
    public function selectCountries(){
        $optionsCountries = Countries::select('id as value', 'description as option')
        ->get();

        return response()->json([
            'status' => true,
            'options'=> $options
        ], 200);
    }

    public function selectAreas(){
        $optionsAreas = Areas::select('id as value', 'description as option')
        ->get();

        return response()->json([
            'status' => true,
            'options'=> $optionsAreas
        ], 200);
    }
}
