<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type_id extends Model
{
    protected $table = 'type_id';
    protected $primaryKey = 'id';
    protected $fillable = ['description'];
}
