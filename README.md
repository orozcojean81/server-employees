# server-employees



## Antes de comenzar

Lo primero que debemos hacer es importar nuestra base de datos.
<p> El archivo .sql de la base de datos está en "base de datos/human_talent.sql" </p>

Instalar las dependencias necesarias con: <strong>composer install</strong>

## Tener en cuenta

La base de datos está en: <strong>Mysql</strong>
Nombre de la base de datos: <strong> human_talent </strong>

# Para iniciar el servidor
Para levantar nuestro servidor solo debemos correr el comando : <strong> php artisan serve </strong>
Se espera que este se inicie en el puerto http://127.0.0.1:8000

# Archivo de rutas

routes/api.php

# Controladores

app/http/controller

