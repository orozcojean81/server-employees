<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => ['cors']], function () {
    Route::post('register-employee', 'EmployeesController@registerEmployee');
    Route::get('list-employees', 'EmployeesController@getEmployees');
    Route::post('create-email', 'EmployeesController@createEmail');
    Route::get('information-employee&id={id}', 'EmployeesController@getInformationByEmployee');
    Route::post('update-information', 'EmployeesController@edditInformationEmployee');
});

